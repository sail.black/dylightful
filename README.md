![Python Versions](https://img.shields.io/badge/python-3.7%20%7C%203.8%20%7C%203.9%20%7C%20-blue) 
![Style Black](https://warehouse-camo.ingress.cmh1.psfhosted.org/fbfdc7754183ecf079bc71ddeabaf88f6cbc5c00/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f64652532307374796c652d626c61636b2d3030303030302e737667) 
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
![Test](https://github.com/MQSchleich/dylightful/actions/workflows/python-app.yaml/badge.svg?branch=main)
[![codecov](https://codecov.io/gh/simonw/asgi-csrf/branch/main/graph/badge.svg)](https://codecov.io/gh/MQSchleich/dylightful/)
[![Documentation Status](https://readthedocs.org/projects/dylightful/badge/?version=latest)](https://dylightful.readthedocs.io/en/latest/?badge=latest)

# Package to develop Markov Analysis of Dynophores

This package aims to make the analysis of interaction patterns in supramolecular complexes more easy. It builds on the concepts of pharmaceutical sciences, computational chemistry, quantum mechanics, statistical modelling, and artificial intelligence. 
Core dependencies are 

* MDAnalysis
* Deeptime 
* Dynophores
* scikit-learn
* hmmlearn
* numpy 
* pandas
* mdtraj

# Install 

To install the necessary packages it is recommended to use the 'conda' package manager. However, all packages can also be installed via the requirements.txt


## User 

```
pip install dylightful
```
## Dev
```
pip install -r requirements.txt
```

# Tutorial 

For a tutorial on how to apply the package to a dynophore check the notebook in [tutorial](https://github.com/MQSchleich/dylightful/tutorial)

For a deep walkthrough checkout the files in [notebooks](https://github.com/MQSchleich/dylightful/notebooks/)


# Documentation

The full documentation is on https://dylightful.readthedocs.io/en/latest/


